/*
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 * You can contact the main authors via email at:
 * Jean-François Gibrat (jean-francois ! gibrat () inra ! fr)
 * Mathématiques et Informatique Appliquées du Génome à l'Environnement (MaIAGE)
 * INRA - Domaine de Vilvert
 * 78350 Jouy-en-Josas cedex
 * France
 * Copyright (C) 2016
 */

/*
 * Structure containing dynamic programming parameters
 */
struct dyn_prog_param
{
  int scores[3];        // scores[0]= match score, scores[1]= mismatch score, scores[2]= gap score
  int sigma;            // sigma is a standard deviation (see function compute_band_width) used to specify the band width
  int margin;           // margin is the number of nucleotides added to the 5' and/or 3' ends of the long read
  int DPband_dim2;      // Second dimension of array DPband (initialized in initialize_band_dyn_prog)
};

/*
 * Structure containing statistics relative to the number of matches in the alignments
 */
struct nber_matches_stat
{
  float Pmatch;      // Probability of a match in the alignment (ignoring second order effects)
  float Ematch;      // Expected value of Nmatch (Nmatch = number of matches in the alignment)
  float Smatch;      // Standard deviation of Nmatch
  float n;           // Number of standard deviations away from Ematch = confidence interval
  float lower_bound; // Lower bound of confidence interval: Ematch - n * Smatch
  int in;            // 1 if Nmatch is within the above lower half of the confidence interval, else 0
};
