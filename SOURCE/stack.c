/*
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 * You can contact the main authors via email at:
 * Jean-François Gibrat (jean-francois ! gibrat () inra ! fr)
 * Mathématiques et Informatique Appliquées du Génome à l'Environnement (MaIAGE)
 * INRA - Domaine de Vilvert
 * 78350 Jouy-en-Josas cedex
 * France
 * Copyright (C) 2017
 */
#define _POSIX_C_SOURCE 200809L

#include <stdio.h>
#include <stdlib.h>
#include "alloc.h"

/*
 * Structure for a stack
 */
struct stack {
  int maxsize;
  int top;
  char **items;
};

// Global variables to this file
static struct stack stk;
static int init = 0;

/********************************************************
 *                                                      *
 ********************************************************/
void stack_initialize(int size)
{

  stk.maxsize = size;
  stk.top = 0;
  if(!init) {
    stk.items = NULL;
    CALLOC2(stk.items,2,size);
    init = 1;
  }
}
/********************************************************
 *                                                      *
 ********************************************************/
void stack_push(char c0, char c1)
{
  if(stk.top == stk.maxsize) {
    fprintf(stderr,"The stack has reached its maximum size (%d)\n",stk.top);
    exit(1);
  }
  stk.items[0][stk.top] = c0; 
  stk.items[1][stk.top] = c1; 
  stk.top++;
}
/********************************************************
 *                                                      *
 ********************************************************/
int stack_pop(char *c0, char *c1)
{
  if(stk.top == 0) {
    return(0);
  }
  stk.top--;
  *c0 = stk.items[0][stk.top]; 
  *c1 = stk.items[1][stk.top]; 
  return(1);
}
/********************************************************
 *                                                      *
 ********************************************************/
void stack_reset()
{
  stk.top = 0;
}
