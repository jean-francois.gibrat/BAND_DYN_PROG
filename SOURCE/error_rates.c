/*
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 * You can contact the main authors via email at:
 * Jean-François Gibrat (jean-francois ! gibrat () inra ! fr)
 * Mathématiques et Informatique Appliquées du Génome à l'Environnement (MaIAGE)
 * INRA - Domaine de Vilvert
 * 78350 Jouy-en-Josas cedex
 * France
 * Copyright (C) 2017
 */
#define _POSIX_C_SOURCE 200809L

#include <stdio.h>
#include <string.h>
#include <math.h>
#include <stdint.h>
#include <stdlib.h>
#include "alloc.h"

char *get_arg_value(char *arg_name);

static float err_rates[3];     // First position is mismatch error rate then insertion then deletion

/****************************************************************
 *                                                              *
 *                                                              *
 ****************************************************************/
void read_error_rates()
{
  /*
   * Reads 3rd generation sequencing technology error rates.
   * There are 3 types of errors : mismatch, insertion and deletion
   */
  FILE *fp;
  char buffer[BUFSIZE];
  float val;
  
  FOPEN(fp,get_arg_value("ErrRates"),"r");
  
  while(fgets(buffer,BUFSIZE,fp) != NULL) {
    if(strncmp(buffer,"# mismatch",10) == 0) {
      fgets(buffer,BUFSIZE,fp);
      sscanf(buffer,"%f",&val);
      if(val > 1.0 || val < 0.0) {
	fprintf(stderr,"Error in read_error_rates: the values of the error rates must lie between 0.0 and 1.0 (%f)\n",val);
	exit(1);
      }
      err_rates[0] = val;
    } else if(strncmp(buffer,"# insertion",11) == 0) {
      fgets(buffer,BUFSIZE,fp);
      sscanf(buffer,"%f",&val);
      if(val > 1.0 || val < 0.0) {
	fprintf(stderr,"Error in read_error_rates: the values of the error rates must lie between 0.0 and 1.0 (%f)\n",val);
	exit(1);
      }
      err_rates[1] = val;
    } else if(strncmp(buffer,"# deletion",10) == 0) {
      fgets(buffer,BUFSIZE,fp);
      sscanf(buffer,"%f",&val);
      if(val > 1.0 || val < 0.0) {
	fprintf(stderr,"Error in read_error_rates: the values of the error rates must lie between 0.0 and 1.0 (%f)\n",val);
	exit(1);
      }
      err_rates[2] = val;
    }
  }

#ifdef DEBUG  
  char err_types[3][10] = {"mismatch ","insertion","deletion "};
  for(int ii = 0; ii < 3; ii++) {
    fprintf(stdout,"%s error rate= %.4f\n",err_types[ii],err_rates[ii]);
  }
#endif

  FCLOSE(fp);

}
/****************************************************************
 *                                                              *
 *                                                              *
 ****************************************************************/
int compute_tolerance(int N, float Nstd)
{
  /*
   * Insertions and deletions in reads randomly modify their length.
   * Without these indels, the distance between the starting
   * positions of the n-th and m-th k-mers (m > n) would be:
   * (m - n) * Lk (where Lk is the length of the k-mers)
   * in both the short and long reads.
   * With indels, these distances are random variables Di and Dj.
   * The difference between these distances, DD, is also a random
   * variable whose mean and variance are:
   * E(DD) = 0
   * Var(DD) = 2N[pm + 4pi - (pm+2pi)^2]
   * where pi is the probability of an insertion and pm is the
   * sum of the probability of a mismatch and the probability of a match.
   *
   * Notice: in theory N should be the distance between the k-mers in reads without indels.
   * In practice, we use the distance between the k-mers in the short read
   * (see function align_short_reads)
   * Nstd is the number of standard deviations one wishes to use for tolerance
   */

  float p_m, p_i;
  float std;
  int tolerance;

  p_m = 1.0 - err_rates[1] - err_rates[2];
  p_i = err_rates[1];
  
  std = sqrt(2*N*(p_m+4*p_i-(p_m+2*p_i)*(p_m+2*p_i)));

  /*
   * The tolerance on the distance difference is Nstd times the standard deviation (rounded to the nearest integer)
   */

  tolerance = (int) (Nstd * std + 0.5);

  return(tolerance);

}

/****************************************************************
 *                                                              *
 *                                                              *
 ****************************************************************/
int compute_band_width(int N)
{
  /*
   * The band width is similar to the standard deviation of the 
   * travelled distance during a 1D random walk of length N
   * See: J-F Gibrat, A short note on dynamic programming in a band
   */
  float pp;
  int sigma;

  pp = 2.0 * (err_rates[1] + err_rates[2] - err_rates[1] * err_rates[1] - err_rates[2] * err_rates[2]);
  sigma = (int) sqrt(2*N*pp);
  return(sigma);

}

/****************************************************************
 *                                                              *
 *                                                              *
 ****************************************************************/
float *get_error_rates()
{
  return(err_rates);
}
