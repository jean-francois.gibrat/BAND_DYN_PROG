/*
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 * You can contact the main authors via email at:
 * Jean-François Gibrat (jean-francois ! gibrat () inra ! fr)
 * Mathématiques et Informatique Appliquées du Génome à l'Environnement (MaIAGE)
 * INRA - Domaine de Vilvert
 * 78350 Jouy-en-Josas cedex
 * France
 * Copyright (C) 2017
 */
#define _POSIX_C_SOURCE 200809L

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdint.h>
#include <stdarg.h>
#include "alloc.h"

static char buffer[BUFSIZE];

char *getLine(FILE *fp, int lvl)
{

/*
 * Read a line in a buffer and *test* whether a buffer overflow occurred.
 * lvl == 0: returns the truncated line _without_ any warning
 * lvl == 1: emits a warning, but returns the truncated line
 * lvl == 2: raise an error and stop
 */

  char *returned_value;

  returned_value = fgets(buffer, BUFSIZE, fp);

  if(lvl > 0 && strlen(buffer) == BUFSIZE - 1) {
    if(lvl == 1) {
      fprintf(stdout,"WARNING in function getLine: the actual buffer size (%d) is too small for the line read.",BUFSIZE);
    } else {
      PRINT_TAG_ERR;
      fprintf(stderr,"The actual buffer size (%d) is too small for the line read. You must redefine the size in alloc.h\n",BUFSIZE);
      exit(1);
    }
  }

  return(returned_value);

}
/*==================================================================================================+
 |                                                                                                  |
 |                                                                                                  |
 +==================================================================================================*/
FILE *Get_File_Ptr(char *fname, char *access_mode)
{

/*
 * This function returns a pointer to a file described in string fname.
 */

  FILE *fp = NULL;

  fp = fopen(fname,access_mode);

  if(fp == NULL) {
    fprintf(stderr,"ERROR in function Get_File_Ptr: unable to find file %s\n\n",fname);
    exit(1);
  }

  return(fp);

}
/*******************************************************************************/
/*                                                                             */
/*                      routine iQsort                                         */
/*                                                                             */
/*******************************************************************************/
void swap(int *indx, int i, int j);

void iQsort(int *v, int *indx, int left, int right)
{

/*
 * sort v[left]...v[right] into increasing order. Note that array v itself is left
 * unmodified and only the pointers to the elements of v, indx are really sorted.
 * Do not forget to initialize the indx array before the first call to iQsort, i.e.,
 *      for(i = 0 ; i < N; i++) {indx[i] = i;}
 *      iQsort(v,indx,0,N-1);
 * where N is the size of v
 */

  int i, last;

  if(left >= right)    /* Do nothing if array contains */
    return;            /* fewer than two elements */

  swap(indx, left, (left+right)/2);  /* move partition elements */
  last = left;                       /* to v[0] */

  for(i = left + 1; i <= right; i++)  /* partition */
    if(v[indx[i]] < v[indx[left]])
       swap(indx, ++last, i);

  swap(indx, left, last);    /* restore partition element */

  iQsort(v, indx, left, last-1);
  iQsort(v, indx, last+1, right);

}


/*******************************************************************************/
/*                                                                             */
/*                      routine rQsort                                         */
/*                                                                             */
/*******************************************************************************/
void swap(int *indx, int i, int j);

void rQsort(float *v, int *indx, int left, int right)
{

/*
 * sort v[left]...v[right] into increasing order. Note that array v itself is left
 * unmodified and only the pointers to the elements of v, indx are really sorted.
 *
 */

  int i, last;

  if(left >= right)    /* Do nothing if array contains */
    return;            /* fewer than two elements */

  swap(indx, left, (left+right)/2);  /* move partition elements */
  last = left;                       /* to v[0] */

  for(i = left + 1; i <= right; i++)  /* partition */
    if(v[indx[i]] < v[indx[left]])
       swap(indx, ++last, i);

  swap(indx, left, last);    /* restore partition element */

  rQsort(v, indx, left, last-1);
  rQsort(v, indx, last+1, right);

}

/*******************************************************************************/
/*                                                                             */
/*                               routine swap                                 */
/*                                                                             */
/*******************************************************************************/

void swap(int *v, int i, int j)
{

/*
 * Interchange indx[i] and indx[j]
 *
 */

  int temp;

  temp = v[i];
  v[i] = v[j];
  v[j] = temp;

}
/*******************************************************************************/
/*                                                                             */
/*                               function chomp                                */
/*                                                                             */
/*******************************************************************************/
int chomp(char *str)
{

/*
 * Get rid of the last '\n' character and return the new string length
 */

  int ll;

  ll = (int) strlen(str);

  if(str[ll-1] != '\n') {
    fprintf(stdout,"Function chomp warning: character string (%s) does not end with a newline character\n\n",str);
    return(ll);
  } else {
    str[ll-1] = '\0';
    return(ll-1);
  }

}

/*******************************************************************************/
/*                                                                             */
/*                            function print_function_header                   */
/*                                                                             */
/*******************************************************************************/
void print_function_header(FILE *fpout, char *title)
{
  char buffer[BUFSIZE];

  strcpy(buffer,"====================================  ");
  strcat(buffer,title);
  strcat(buffer,"  ====================================");
  fprintf(fpout,"\n\n\n%s\n\n",buffer);

}

/*******************************************************************************/
/*                                                                             */
/*                            function Error                                   */
/*                                                                             */
/*******************************************************************************/
void Error(char *fmt, ...)
{
   va_list args;

/*
 * Print a message on stderr and exit with code 1
 */

   PRINT_TAG_ERR;
   va_start(args, fmt);
   vfprintf(stderr, fmt, args);
   va_end(args);

   exit(1);

}

#ifdef DEBUG
/*******************************************************************************/
/*                                                                             */
/*                         print_debug functions                               */
/*                                                                             */
/*******************************************************************************/
// Global variable to the end of this file
static int print_status = 0;

void set_print_debug(int val)
{
  print_status = val;
}

int print_debug()
{
  return(print_status);
}
#endif
