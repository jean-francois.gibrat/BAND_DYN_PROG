/*
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 * You can contact the main authors via email at:
 * Jean-François Gibrat (jean-francois ! gibrat () inra ! fr)
 * Mathématiques et Informatique Appliquées du Génome à l'Environnement (MaIAGE)
 * INRA - Domaine de Vilvert
 * 78350 Jouy-en-Josas cedex
 * France
 * Copyright (C) 2017
 */
#define _POSIX_C_SOURCE 200809L

#include <stdio.h>
#include <string.h>
#include <math.h>
#include <stdint.h>
#include <stdlib.h>
#include <ctype.h>
#include "alloc.h"

// Prototypes of functions called by main
void process_line_args(int argc, char *argv[]);
void read_error_rates();
void initialize_band_dyn_prog();
int band_dyn_prog(char *LRseq, int LRlen, char *SRseq, int SRlen);
void read_DP_scores();
int get_band_max_score();
char **get_band_ali();
void read_sequence_file(char **seq1, int *len1, char **seq2, int *len2);

int main(int argc, char *argv[])
{

  char *seq1 = NULL, *seq2 = NULL;
  int len1, len2;
  
  process_line_args(argc,argv);

  /*
   * Input the sequences
   */  
  read_sequence_file(&seq1,&len1,&seq2,&len2);

  /*
   * Read 3rd generation sequencing technology error rates
   */
  read_error_rates();

  /*
   * Read the dynamic programming parameters
   */
  read_DP_scores();
  
  /*
   * Dynamic programming alignment in a band
   */
  initialize_band_dyn_prog();
  
  int retval = band_dyn_prog(seq1,len1,seq2,len2);

  if(retval) {
    int max_score = get_band_max_score();
    char **ali = get_band_ali();
    fprintf(stdout,"Alignment score= %d\n",max_score);
    fprintf(stdout,"%s <-- seq1\n",ali[0]);
    fprintf(stdout,"%s <-- seq2\n",ali[1]);
  }

  FREE(seq1);
  FREE(seq2);
  
  return(0);

}

/**************************************************
 *                                                *
 *                                                *
 **************************************************/
char *get_arg_value(char *arg_name);
FILE *Get_File_Ptr(char *fname, char *access_mode);
char *getLine(FILE *fp, int lvl);
int chomp(char *str);

//Global variables to the end of this file
static int max1 = -1;
static int max2 = -1;

void read_sequence_file(char **seq1, int *len1, char **seq2, int *len2)
{
  /*
   * This function expects a fasta file with 2 sequences, each on a single line
   * > title seq 1
   * AGCATTA....
   * > title seq2
   * CTACTGA...
   */
  FILE *fp;
  char *buffer;

  fp = Get_File_Ptr(get_arg_value("SeqInputFile"),"r");
  for(int i = 1; i <= 2; i++) {
    buffer = getLine(fp,2);
    if(buffer[0] != '>') {
      PRINT_TAG_ERR;
      fprintf(stderr,"Expecting a fasta title line starting with '>'\n%s\n\n",buffer);
      exit(1);
    }
    buffer = getLine(fp,2);
    int len = chomp(buffer);
    if(i == 1) {
      *len1 = len;
      max1 = *len1+2;
      CALLOC(*seq1,max1);
      (*seq1)[0] = '@';
      strcpy((*seq1)+1,buffer);
      for(int k = 1; k <= *len1; k++) {
	(*seq1)[k] = toupper((*seq1)[k]);
      }
    } else {
      *len2 = len;
      max2 = *len2+2;
      CALLOC(*seq2,max2);
      (*seq2)[0] = '@';
      strcpy((*seq2)+1,buffer);
      for(int k = 1; k <= *len2; k++) {
	(*seq2)[k] = toupper((*seq2)[k]);
      }
    }
  }
  
}
/**************************************************
 *                                                *
 *                                                *
 **************************************************/
int longest_LR()
{
  return(max1);
}
int longest_SR()
{
  return(max2);
}

