/*
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 * You can contact the main authors via email at:
 * Jean-François Gibrat (jean-francois ! gibrat () inra ! fr)
 * Mathématiques et Informatique Appliquées du Génome à l'Environnement (MaIAGE)
 * INRA - Domaine de Vilvert
 * 78350 Jouy-en-Josas cedex
 * France
 * Copyright (C) 2017
 */
#define _POSIX_C_SOURCE 200809L

#include <stdio.h>
#include <string.h>
#include <math.h>
#include <stdint.h>
#include <stdlib.h>
#include <limits.h>
#include <ctype.h>
#include "alloc.h"
#include "struct.h"

// Global variables to this file
static int maxval;                 // Alignement optimal score
static int **DPband;               // The dynamic programming matrix as a band (SRlen+1, 2*band_width+2)
static int band_dim2;              // DPband[longest_SR+1][band_dim2]
static char **alignment;           // Alignment of the long read with the short read
                                   // alignment[0][...] contains the (edited) long read sequence,
                                   // alignment[1][...] contains the (edited) short read sequence
static int ali_dim2;               // alignment[2][ali_dim2]

/************************************************************************************************************
 *                                                                                                          *
 *                                           BAND_DYN_PROG                                                  *
 *                                                                                                          *
 ************************************************************************************************************/
// Prototypes of functions called by function band_dyn_prog
void stack_push(char c0, char c1);
int stack_pop(char *c0, char *c1);
int in_confIntvl(int Nmatches, int SRlen);
int compute_band_width(int N);
void realloc_DPband(int new_dim);
struct dyn_prog_param *get_dyn_prog_param();

int band_dyn_prog(char *LRseq, int LRlen, char *SRseq, int SRlen)
{
  /*
   * LRseq: first sequence
   * LRlen: first sequence length
   * SRseq: second sequence
   * SRlen: second sequence length
   * Notice: the sequences must start at position 1, position 0 contains a dummy symbol such as '@'
   */
  int *scores;                // scores[0]= match score, scores[1]= mismatch score, scores[2]= gap score
  int margin;                 // Number of nucleotides added to the 3' and 5' ends of the LR seq to take into account indels due to sequencing errors
  int offset;                 // offset = k_band + 1 - margin
  int k_band;                 // The band has width 2 * k_band + 1
  int Nmatches;               // Number of matches in the alignment
  int Ntries;                 // Number of times the bandwidth standard deviation sigma is increased
  int reachedBandEdge;        // 1 if alignment reached the edge of the band, else 0
  int band_alignment_OK;      // 1 if the DP algorithm in a band has found the optimal score, else 0
  int prev_Nmatches;
  int prev_maxval;
  int ii, jj, hh;
  int i1, j1;
  int i, j;
  int val;
  int maxi1, maxj1;
  char c0, c1;
  int i1_start, j1_start;
  struct dyn_prog_param *dpp;

  /*
   * Check that the sequences start at position 1
   */
  char letter1 = toupper(LRseq[0]);
  char letter2 = toupper(SRseq[0]);
  if(letter1 == 'A' || letter1 == 'C' || letter1 == 'G' || letter1 == 'T' ||
     letter2 == 'A' || letter2 == 'C' || letter2 == 'G' || letter2 == 'T') {
    PRINT_TAG_ERR;
    fprintf(stderr,"Sequences must start at position 1. Position 0 should contain a dummy symbol, e.g., '@'\n\n");
    exit(1);
  }

    /*
   * Get the parameters of the dynamic programmming algorithm
   * Notice: margin is set to 0 in function LR_excision
   */
  dpp = get_dyn_prog_param();  
  scores = dpp->scores;
  margin = dpp->margin;
  k_band = compute_band_width(SRlen);
  if(2*k_band+2 > band_dim2) {
    realloc_DPband(2*k_band+2);
  }
  dpp->sigma = k_band;
  
  prev_maxval = -1;
  prev_Nmatches = -1;

  /*
   * One needs to check whether the aligment path remains within the band. One starts with a band having
   * a width 2*sigma+1 and checks if the alignment path remains in the band. If it does not, sigma is doubled
   * and the dynamic programming algorithm in a band is started anew. If required, this processus is performed
   * up to 3 times (i.e., up to 3 sigmas). After these 3 tries, if the path still does not remain in the band, 
   * the pair of reads is considered to be too dissimilar to be aligned and the program gives up for this pair.
   */
  Ntries = 1;
  do {
    /*
     * Reset to zero the required matrix elements (needed when the function is called several times in a row)
     */
    offset = k_band + 1 - margin;
    i1 = 1;
    j1 = offset;                 // Mapping of point ii = 1 and jj = 1 in DPband (see below)
    for(i = i1, j = j1-1; j > 0 && i <= SRlen; j--, i++) {    // 'upper left corner' diagonal
      DPband[i][j] = 0;
    }

    for(j = 1; j <= 2*k_band+1; j++) { // Last row
      DPband[SRlen][j] = 0;
    }
  
    i1 = SRlen;
    j1 = offset + LRlen - SRlen; // Mapping of point ii = SRlen and jj = LRlen in DPband
    for(i = i1, j = j1; j <= 2*k_band+1 && i > 0; j++, i--) { // 'lower right corner' diagonal
      DPband[i][j] = 0;
    }

    /*
     *          ===============================================
     *          | 1) Fill the dynamic programming band-matrix |
     *          ===============================================
     * 
     *
     * Note: long read ==> index jj and short read ==> index ii in the regular (SRlen+1 x LRlen+1) DP matrix
     * These 2 indexes are transformed into indexes i1, j1 in the new (2*k_band+2 x SRlen+1) DP band-matrix 
     * The mapping is the following: 
     * j1 = ii                 <==>  ii = j1
     * i1 = offset + jj - ii   <==>  jj = j1 + i1 - offset
     * with offset = k_band + 1 - margin
     * 
     * The diagonal move in regular DP algorithm (match or mismatch) becomes a vertical move, i.e.,
     * (ii-1, jj-1) ==> (i1-1, j1)
     * The horizontal move in regular DP algorithm (insertion in LR seq.) stays a horizontal move, i.e.,
     * (ii, jj-1)   ==> (i1, j1-1)
     * The vertical move in regular DP algorithm (deletion in LR seq.) becomes a diagonal move, i.e.,
     * (ii-1, jj)   ==> (i1-1, j1+1)
     * 
     * LR sequence   TCTAAGTCAGATCAT    TCTAAG-CAGATCAT
     * SR sequence   TCTAAG-CAGATCAT    TCTAAGTCAGATCAT    
     *               insertion in LR     deletion in LR
     * or conversely  deletion in SR    insertion in SR
     */

    for(ii = 1; ii <= SRlen; ii++) {
      for(hh = -k_band; hh <= +k_band; hh++) {
	jj = ii + hh + margin;

	if(1 <= jj && jj <= LRlen) {
	  i1 = ii;
	  j1 = offset + jj - ii;

	  // Vertical move corresponds to the diagonal move in the regular DP matrix
	  DPband[i1][j1] = DPband[i1-1][j1] + ((SRseq[ii] == LRseq[jj]) ? scores[0] : scores[1]);
	  
	  // Diagonal move corresponds to the vertical move in the regular DP matrix
	  if(j1+1 < 2 * k_band + 2) {  // Check that element [i1-1][j1+1] is inside the band
	    val = DPband[i1-1][j1+1] + scores[2];
	    if(val > DPband[i1][j1]) {
	      DPband[i1][j1] = val;
	    }
	  }

	  // Horizontal move corresponds to the horizontal move in the regular DP matrix
	  if(j1-1 > 0) {                 // Check that element [i1][j1-1] is inside the band
	    val = DPband[i1][j1-1] + scores[2];
	    if(val > DPband[i1][j1]) {
	      DPband[i1][j1] = val;
	    }
	  }
	  
	}
      } // End of loop index hh
    }   // End of loop index ii

    /*
     *          =======================================================
     *          | 2) Look for the largest value in the band DP matrix |
     *          =======================================================
     *
     * Note: in the regular (SRlen+1 x LRlen+1) DP matrix, the largest value is to be searched
     * for in the last row and the last column starting at position [SRlen][LRlen]
     * In the (2*k_band+2 x SRlen+1) band matrix,starting from the mapping of [SRlen][LRlen],
     * the largest value is searched for in the last row and the 'lower right corner' diagonal
     * If margin > k_band, elements of the band never reach the last column (LRlen)
     */
    i1_start = SRlen;
    j1_start = offset + LRlen - SRlen;
    if(j1_start > 2*k_band+1) {
      j1_start  = 2*k_band+1;
    }
    maxval = DPband[i1_start][j1_start];
    maxi1 = i1_start;
    maxj1 = j1_start;

    for(j1 = j1_start-1; j1 > 0; j1--) { // Last row
      if(DPband[maxi1][j1] > maxval) {
	maxval = DPband[maxi1][j1];
	maxj1 = j1;
      }
    } 

    // If the max value is found below, this means that the short read could not be aligned over its whole length
    // If margin > k_band, the program never goes through the loop below
    for(i1 = i1_start-1, j1 = j1_start+1; j1 <= 2*k_band+1 && i1 > 0; i1--, j1++) { // 'lower right corner' diagonal
      if(DPband[i1][j1] > maxval) {
	maxval = DPband[i1][j1];
	maxi1 = i1;
	maxj1 = j1;
      }
    }

    /*
     *          ============================================
     *          | 3) Back track to determine the alignment |
     *          ============================================
     */

    // Take care of 3' dangling sequence
    if(maxi1 == SRlen) {
      int jj_end = maxj1 + maxi1 - offset;
      for(jj = LRlen; jj > jj_end; jj--) {
	stack_push(LRseq[jj],'-');
      }
    } else { // Only if margin < k_band
      for(i1 = i1_start, j1 = j1_start; i1 > maxi1; i1--, j1++) {
	ii = i1;
	stack_push('-',SRseq[ii]);
      }
    }

    // Follow the path in the matrix until the first column is reached
    int mapi = 1, mapj = offset;   // Mapping of point ii=1 & jj=1
    float slope;
    int uld;
    int j1_max = 0;
    int j1_min = INT_MAX;

    i1 = maxi1;
    j1 = maxj1;
    if(j1_max < j1) j1_max = j1;
    if(j1_min > j1) j1_min = j1;
    uld = 0;

    while(j1 > 0 && i1 > 0 && !uld) {
      slope = (float)(i1 - mapi) / (float)(j1 - mapj);
      /* 
       * When the following test is true, the diagonal in the upper left corner of the band matrix has been reached.
       * This diagonal marks the boundary of the band matrix (if offset > 0). If the path crosses this diagonal, the
       * program exits the while loop, else it continues along the diagonal.
       */
      if(fabs(slope+1) < 0.001) { 
	uld = 1;                  
      }                           
      ii = i1;
      jj = j1 + i1 - offset;
      
      if(DPband[i1][j1] == DPband[i1-1][j1] + ((SRseq[ii] == LRseq[jj]) ? scores[0] : scores[1])) {
	// Diagonal move in the regular DP matrix
	stack_push(LRseq[jj],SRseq[ii]);
	i1 = i1 - 1;

      } else if(j1+1 < 2 * k_band + 2 && DPband[i1][j1] == DPband[i1-1][j1+1] + scores[2]) {
	// Vertical move in the regular DP matrix
	stack_push('-',SRseq[ii]);
	if(uld) uld = 0;  // We have reached the upper left diagonal, but since we are moving along it we do not leave the while loop
	i1 = i1 - 1;
	j1 = j1 + 1;
	if(j1_max < j1) j1_max = j1;
	
      } else if(j1-1 > 0 && DPband[i1][j1] == DPband[i1][j1-1] + scores[2]) {
	// Horizontal move in the regular DP matrix
	stack_push(LRseq[jj],'-');
	j1 = j1 - 1;
	if(j1_min > j1) j1_min = j1;

      } else {
	fprintf(stderr,"ERROR in function band_dyn_prog while back tracking from position (i=%d j=%d)\n",maxi1,maxj1);
	fprintf(stderr,"The problem occurred at indices i=%d j=%d,\n",i1,j1);
	fprintf(stderr,"In theory, you should never see this message. Get in touch with the developer\n\n");
	exit(1);
      }

    }
  
    // Take care of 5' dangling sequence
    if(i1 == 0) {
      int jj_start = i1 + j1 - offset;
      for(jj = jj_start; jj > 0; jj--) {
	stack_push(LRseq[jj],'-');
      }
    } else {  // Only if margin < k_band
      int ii_start = i1 - 1;
      if(uld) ii_start = i1;
      for(ii = ii_start; ii > 0; ii--) {
	stack_push('-',SRseq[ii]);
      }
    }

    /*
     * Fill alignment array (alignment[0] contains the LR sequence)
     */
    Nmatches = 0;
    hh = 0;
    alignment[0][hh] = '@';
    alignment[1][hh] = '@';
    while(stack_pop(&c0,&c1)) {
      hh++;
      alignment[0][hh] = c0;
      alignment[1][hh] = c1;
      if(c1 == c0) Nmatches++;
    }
    hh++;
    alignment[0][hh] = '\0';
    alignment[1][hh] = '\0';

    /*
     * Check whether the aligment path remained within the band.
     *
     * We first check whether the path reaches the edge of the band.
     * 1) If it does: we restart the alignment with a bandwidth increased by sigma
     * 2) If it does not: we further check whether the % of matches in the alignment
     *    is outside the confidence interval (see function in_confIntvl)
     *    If it is outside, we restart the alignement with an increased bandwidth
     */

    if(j1_min == 1 || j1_max == 2 * k_band + 1) {
      reachedBandEdge = 1;
    } else {
      reachedBandEdge = 0;
    }

    if(reachedBandEdge) {
      band_alignment_OK = 0;
    } else {
      if(in_confIntvl(Nmatches,SRlen)) {
	band_alignment_OK = 1;
      } else {
	band_alignment_OK = 0;
      }
    }

    if(! band_alignment_OK) {
      // Increase the band width by sigma (less than 0.3% of the comparisons should require more than 3 sigmas)
      Ntries++;
      if(Ntries > 3 || (prev_Nmatches == Nmatches && prev_maxval == maxval)) {
	fprintf(stdout,"\tFunction band_dyn_prog: giving up alignment after %d tries ",Ntries-1);
	if(reachedBandEdge) {
	  fprintf(stdout,"due to reaching band edge\n");
	} else {
	  fprintf(stdout,"due to insufficient number of matches (%d/%d)\n",Nmatches,SRlen);
	}
	return(0);
      }
      fprintf(stdout,"\tFunction band_dyn_prog: adding sigma= %d to the band width= %d\n",
	      dpp->sigma, k_band);
      k_band = Ntries * dpp->sigma;
      if(2*k_band+2 > band_dim2) {
	realloc_DPband(2*k_band+2);
      }
      prev_maxval = maxval;
      prev_Nmatches = Nmatches;
    }

  } while(! band_alignment_OK);

  return(1);

}

/************************************************************************************************************
 *                                                                                                          *
 ************************************************************************************************************/
// Prototypes of functions called by initialize_band_dyn_prog
char *get_arg_value(char *arg_name);
int longest_LR();
int longest_SR();
void read_DP_scores();
void stack_initialize(int size);
void initialize_in_confIntvl();
int compute_band_width(int N);

// Global variable to the end of this file
struct dyn_prog_param dpp;

void initialize_band_dyn_prog()
{
  int max_k_band;

  initialize_in_confIntvl();
  read_DP_scores();

  dpp.margin = 0;
  max_k_band = compute_band_width(longest_SR());
  band_dim2 = 6 * max_k_band + 2;               // max_k_band can be increased up to 3 times: 2 * (3 * k_band) + 2
  CALLOC2(DPband,longest_SR()+2,band_dim2);

  ali_dim2 = longest_SR() + longest_LR() + 1;
  CALLOC2(alignment,2,ali_dim2);

  stack_initialize(ali_dim2);

}

/************************************************************************************************************
 *                                                                                                          *
 ************************************************************************************************************/
int longest_SR();

void realloc_DPband(int new_dim)
{
  FREE2(DPband);
  band_dim2 = new_dim;
  CALLOC2(DPband,longest_SR()+2,band_dim2);
}

/************************************************************************************************************
 *                                                                                                          *
 ************************************************************************************************************/
// prototype of functions called by read_DP_scores
FILE *Get_File_Ptr(char *fname, char *access_mode);
char *getLine(FILE *fp, int lvl);

void read_DP_scores()
{
  FILE *fp;
  char *buffer;
  char type[100];
  int itmp;

  fp = Get_File_Ptr(get_arg_value("DPscores"),"r");

  while((buffer = getLine(fp,2)) != NULL) {
    if(buffer[0] == '#') continue;
    sscanf(buffer,"%s %d",type,&itmp);
    if(strcmp(type,"match") == 0) {
      dpp.scores[0] = itmp;
    } else if(strcmp(type,"mismatch") == 0) {
      dpp.scores[1] = itmp;
    } else if(strcmp(type,"gap") == 0) {
      dpp.scores[2] = itmp;
    } else {
      fprintf(stderr,"Invalid type for the dynamic programming scores (%s)\n",type);
      fprintf(stderr,"The type should be either 'match', 'mismatch' or 'gap'\n");
      fprintf(stderr,"Modify file %s accordingly\n\n",get_arg_value("DPscores"));
      exit(1);
    }
  }

  FCLOSE(fp);
}
/*********************************************************
 *                                                       *
 *                                                       *
 *********************************************************/
float *get_error_rates();

static struct nber_matches_stat nms;

void initialize_in_confIntvl()
{
  float *err_rates;

  err_rates = get_error_rates();
  nms.n = 3.0;   // We consider 3 sigmas (99% confidence interval)
  nms.Pmatch = 1.0 - err_rates[0] - err_rates[1] - err_rates[2];
}

/***********************************<**********************
 *                                                       *
 *                                                       *
 *********************************************************/
int in_confIntvl(int Nmatches, int SRlen)
{
  /*
   * Test whether the alignment is _likely_ to have left the band.
   * If the number of matches in the alignment is smaller than
   * lower_bound below, i.e., the expected number of matches minus 
   * 'n' standard deviations, it is considered outside the band.
   */

  nms.Ematch = SRlen * nms.Pmatch;
  nms.Smatch = sqrt(SRlen * nms.Pmatch * (1. - nms.Pmatch));
  nms.lower_bound = nms.Ematch - nms.n * nms.Smatch;
  nms.in = Nmatches >= nms.lower_bound;

  return(nms.in);

}

/**********************************************
 *                                            *
 **********************************************/
void free_band_DP_arrays()
{
  FREE2(DPband);
  FREE2(alignment);
}

/**********************************************
 *                                            *
 **********************************************/
struct dyn_prog_param *get_dyn_prog_param()
{
  return(&dpp);
}

int get_band_max_score()
{
  return(maxval);
}

int **get_band_matrix()
{
  return(DPband);
}

char  **get_band_ali()
{
  return(alignment);
}
